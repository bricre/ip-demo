import Vue from 'vue'
import App from './App'

import cusImg from '@/components/cus-img.vue'
Vue.component('cus-img', cusImg)

Vue.config.productionTip = false

Vue.prototype.sourceUrl = 'https://static.ipandarin.com/'

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
